<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="initial-scale=1.0, width=device-width">
    <title>Главная
    </title>
  <link href="./custom-common-js.css" rel="stylesheet"></head>
  <body>
<header>
  <div class="container">
    <div class="row">
      <div class="col-md-3 col-sm-3 col-xs-3 header-logo"><a href="index.html"><img src="./images/logo.png"></a></div>
      <div class="col-md-4 col-sm-3 col-xs-2"></div>
      <div class="col-md-3 col-sm-4 col-xs-5 header-phone">
        <p class="header-phone-number"><a href="tel:+88002006203">8-800 200-6-203</a></p>
        <p class="header-phone-text">звонок по России бесплатный</p>
      </div>
      <div class="col-md-1 col-sm-1 col-xs-2 header-backet"><a href="basket-page.html"><img src="./images/basket-top.svg"><span class="header-basket-items"></span></a></div>
    </div>
  </div>
</header>
<div class="anketa">
  <div class="container">
    <h2 class="anketa-name"><span><a href=""><img src="./images/arrow.png"></a></span><span class="anketa-name-text">Оформление заказа</span></h2>
    <div class="row anketa-step-1">
      <div class="anketa-step-line"></div>
      <ul class="anketa-step-menu">
        <li class="anketa-step-menu-active" id="1">Основная информация</li>
        <li id="2">Выбор POS-терминала</li>
        <li id="3">Обороты и комиссия</li>
        <li id="4">Анкета организации</li>
        <li id="5">Анкета клиента</li>
        <li id="6">Оплата</li>
      </ul>
    </div>
  </div>
</div>
<!--Шаг 1. Основная информация-->
<form id="anketa-wrap">
  <div id="anketa">
    <h2>Основная информация</h2>
    <div class="step" data-step="1">
      <div class="container">
        <div class="wrapper-text">
          <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6">
              <div class="select-place"><span class="input-text">Юридическое название органицзации</span>
                <select class="anketa-select" name="type-org" multiple="multiple"></select>
              </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-6">
              <div class="input-place"><span class="input-text">Юридическое название органицзации</span>
                <input class="anketa-input" type="text" name="FIO" placeholder="Юридическое название организации">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-6">
              <select class="anketa-select" multiple="multiple" data-placeholder="Город" name="city"></select>
            </div>
          </div>
        </div>
      </div>
      <div class="anketa-other-back">
        <div class="container">
          <div class="wrapper-text">
            <div class="row">
              <div class="col-md-12">
                <h4>Персональная информация</h4>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="input-place"><span class="input-text">Фамилия Имя и Отчество подписанта</span>
                  <input class="anketa-input" type="text" name="FIO" placeholder="Фамилия Имя и Отчество подписанта">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="input-place"><span class="input-text">Электронная почта</span>
                  <input class="anketa-input" type="text" name="email" placeholder="Электронная почта"><span class="anketa-input-help">Адрес электронной почты в последствии будет являться логином в Ваш личный кабинет.</span>
                </div>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-6">
                <div class="input-place"><span class="input-text">Телефон</span>
                  <input class="anketa-input" type="number" name="phone" placeholder="Телефон">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="anketa-next-step-btn">
        <div class="container">
          <div class="wrapper-text">
            <div class="row">
              <div class="col-md-6 col-sm-6 col-xs-6"></div>
              <div class="col-md-6 col-sm-6 col-xs-6"><span class="anketa-small-text">Нажимая кнопку «ДАЛЕЕ», я соглашаюсь с<a href="#"> передачей моих данных.</a></span>
                <button class="btn-next" data-target="anketa-btn">Далее</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Шаг 2. Выбор терминала-->
    <h2>Выбор POS-терминала</h2>
    <div class="step" data-step="2">
      <div class="container">
        <div class="wrapper-text">
          <div class="row">
            <div class="col-md-6">
              <h4>Есть собственный терминал?</h4>
              <div class="switch">
                <div class="quality" data-target="anketa-answer-yes">
                  <input id="q1" checked="true" name="q" type="radio" value="q1">
                  <label for="q1">Да</label>
                </div>
                <div class="quality" data-target="anketa-answer-no">
                  <input id="q2" name="q" type="radio" value="q2">
                  <label for="q2">Нет</label>
                </div>
              </div>
            </div>
          </div>
          <div class="anketa-row" id="anketa-answer-yes">
            <div class="row">
              <div class="col-md-6">
                <select class="anketa-select" name="type-terminal" data-placeholder="Выберите терминал" multiple="multiple"></select>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="input-place"><span class="input-text">Серийный номер терминала</span>
                  <input class="anketa-input" type="text" name="terminal-number" placeholder="Серийный номер терминала"><span class="anketa-input-help">Если у вас несколько терминалов данного типа, укажите серийные номера через запятую.</span>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-8"><span class="anketa-help">Настройка POS-терминала осуществляется банком.<br>За настройку каждого POS-терминала взимается комиссия в размере 1000 рублей, включая НДС.</span></div>
            </div>
          </div>
          <div class="anketa-row display-none" id="anketa-answer-no">
            <div class="row">
              <div class="col-md-6">
                <select class="anketa-select" name="type-connection" data-placeholder="Тип подключения" multiple="multiple"></select>
                <div class="wrapper-circle-help"><span class="anketa-choose-help">?</span><span class="anketa-choose-help-text">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut</span></div>
              </div>
            </div>
            <div class="row row-content">
              <div class="col-md-3"></div>
              <div class="col-md-5"><span class="anketa-name-col">ТЕРМИНАЛ</span></div>
              <div class="col-md-2"><span class="anketa-name-col anketa-name-col-center">КОЛИЧЕСТВО</span></div>
              <div class="col-md-2"><span class="anketa-name-col anketa-name-col-center">СТОИМОСТЬ</span></div>
            </div>
            <div class="row anketa-item-row anketa-item-row-item-1">
              <div class="anketa-item">
                <div class="col-md-3">
                  <div class="anketa-shop">
                    <input name="anketa-step-2-answer" type="checkbox" value="anketa-step-2-yes" id="terminal-answer">
                    <label for="terminal-answer">терминал</label>
                  </div>
                </div>
                <div class="col-md-5"><span class="anketa-name-terminal">Ingenico IWL<span>МОБИЛЬНЫЙ</span></span><span class="anketa-name-terminal-info" data-toggle="modal" data-target="#myModal1">Подробнее</span></div>
                <div class="col-md-2"><span class="anketa-change-kol anketa-change-delete">&mdash;</span><span class="anketa-change-item">0</span><span class="anketa-change-kol anketa-change-add">+</span></div>
                <div class="col-md-2"><span class="anketa-totalcost" data-price="22000"> 22000</span><span class="anketa-totalcost-rubles"> &#8381;</span><span class="anketa-totalcost-formula"></span></div>
              </div>
            </div>
            <div class="modal fade" id="myModal1" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button class="close" type="button" data-dismiss="modal" aria-hidden="true">×</button><span class="modal-title anketa-name-terminal">Ingenico IWL<span> МОБИЛЬНЫЙ</span></span>
                    <div class="anketa-item-pic">
                      <div class="anketa-item-pic-curr">Картинк</div>
                      <div class="anketa-item-pic-curr">Картинка</div>
                      <div class="anketa-item-pic-curr">Картинка</div>
                    </div>
                  </div>
                  <div class="modal-body">
                    <div class="row anketa-item-descrip">
                      <div class="wrapper-text">
                        <div class="col-md-5 anketa-item-descrip-main">
                          <div class="anketa-item-descrip-main-info">
                            <div class="wrapper-text-mini">
                              <div class="row row-content">
                                <div class="col-md-6"><span class="modal-middle-text">Тип подключения:
                                    <div class="anketa-wrapper-img"><span class="anketa-wrapper-znak">?</span></div></span></div>
                                <div class="col-md-6"><span class="modal-middle-text-2">GPRS</span></div>
                              </div>
                              <div class="row row-content">
                                <div class="col-md-6"><span class="modal-middle-text">Наличие ПИН-пада:
                                    <div class="anketa-wrapper-img"><span class="anketa-wrapper-znak">?</span></div></span></div>
                                <div class="col-md-6"><span class="modal-middle-text-2">Внутренний</span></div>
                              </div>
                              <div class="row row-content">
                                <div class="col-md-6"><span class="modal-middle-text">Сфера применения:</span></div>
                                <div class="col-md-6"><span class="modal-middle-text-2">Рестораны, доставка, страхование и др.</span></div>
                              </div>
                            </div>
                          </div>
                          <div class="anketa-item-descrip-main-pay">
                            <div class="wrapper-text-mini"><span class="anketa-item-descrip-main-pay-result">19 500 &#8381;</span>
                              <div class="anketa-btn-form">
                                <button class="btn-form">Выбрать</button>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-7 anketa-item-descrip-addit">
                          <div class="wrapper-text-mini"><span class="descrip-name">Описание:
                              <div class="wrapper-descrip"><span class="descrip-text">Беспроводной POS-терминал для мобильных продаж/услуг.</span></div></span><span class="descrip-name">Основные преимущества:
                              <div class="wrapper-descrip"><span class="descrip-text">Гарантия 2 года; <br></span><span class="descrip-text">Самый маленький и легкий терминал в классе беспроводных платежных устройств; <br></span><span class="descrip-text">Стоимость включена сим-карта на 2 месяцев.</span></div></span><span class="descrip-name">Технические характеристики:
                              <div class="wrapper-descrip"><span class="descrip-text">Сертифицирован PCI PED 2.x; <br></span><span class="descrip-text">Все виды ридеров:<span class="descrip-text-info"> магнитный, чип, бесконтактный; <br></span></span><span class="descrip-text">Связь:<span class="descrip-text-info"> оффлайн, Bluetooth, GPRS, 3G; <br></span></span><span class="descrip-text">Батарея:<span class="descrip-text-info"> Li-Ion 2050 mA/h с ресурсом до 400 транзакций;<br></span></span><span class="descrip-text">Дисплей:<span class="descrip-text-info"> монохром или полноцветный;<br></span></span><span class="descrip-text">Клавиатура:<span class="descrip-text-info"> рельефная с подсветкой.</span></span></div></span><span class="descrip-name">Поставщик:
                              <div class="wrapper-descrip"><span class="descrip-text">ООО &laquo; ПЛАТИНА &raquo;</span></div></span></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer"><span class="anketa-help">
                      Настройка POS-терминала осуществляется банком.
                      За настройку каждого <br> POS-терминала взимается комиссия в размере 1000 рублей, включая НДС.</span></div>
                </div>
              </div>
            </div>
            <div class="row anketa-item-row anketa-item-row-item-2">
              <div class="anketa-item">
                <div class="col-md-3">
                  <div class="anketa-shop">
                    <input name="anketa-step-2-answer" type="checkbox" value="anketa-step-2-yes" id="terminal-answer-2">
                    <label for="terminal-answer-2">терминал</label>
                  </div>
                </div>
                <div class="col-md-5"><span class="anketa-name-terminal">Ingenico IWL<span>МОБИЛЬНЫЙ</span></span><span class="anketa-name-terminal-info" data-toggle="modal" data-target="#myModal2">Подробнее</span></div>
                <div class="col-md-2"><span class="anketa-change-kol anketa-change-delete">&mdash;</span><span class="anketa-change-item">3</span><span class="anketa-change-kol anketa-change-add">+</span></div>
                <div class="col-md-2"><span class="anketa-totalcost" data-price="17500"> 17500</span><span class="anketa-totalcost-rubles"> &#8381;</span><span class="anketa-totalcost-formula">3 x 17500 &#8381;</span></div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-7"><span class="anketa-help">Настройка POS-терминала осуществляется банком.<br>За настройку каждого POS-терминала взимается комиссия в размере 1000 рублей, включая НДС.</span></div>
              <div class="col-md-5"><span class="anketa-pay-total">Итого 4 терминала на сумму<span class="anketa-pay-total-rubles"> 80500 &#8381;</span></span></div>
            </div>
            <div class="modal fade" id="myModal2" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button class="close" type="button" data-dismiss="modal" aria-hidden="true">×</button><span class="modal-title anketa-name-terminal">Ingenico IWL<span> МОБИЛЬНЫЙ</span></span>
                    <div class="anketa-item-pic">
                      <div class="anketa-item-pic-curr">Картинк</div>
                      <div class="anketa-item-pic-curr">Картинка</div>
                      <div class="anketa-item-pic-curr">Картинка</div>
                    </div>
                  </div>
                  <div class="modal-body">
                    <div class="row anketa-item-descrip">
                      <div class="wrapper-text">
                        <div class="col-md-5 anketa-item-descrip-main">
                          <div class="anketa-item-descrip-main-info">
                            <div class="wrapper-text-mini">
                              <div class="row row-content">
                                <div class="col-md-6"><span class="modal-middle-text">Тип подключения:
                                    <div class="anketa-wrapper-img"><span class="anketa-wrapper-znak">?</span></div></span></div>
                                <div class="col-md-6"><span class="modal-middle-text-2">GPRS</span></div>
                              </div>
                              <div class="row row-content">
                                <div class="col-md-6"><span class="modal-middle-text">Наличие ПИН-пада:
                                    <div class="anketa-wrapper-img"><span class="anketa-wrapper-znak">?</span></div></span></div>
                                <div class="col-md-6"><span class="modal-middle-text-2">Внутренний</span></div>
                              </div>
                              <div class="row row-content">
                                <div class="col-md-6"><span class="modal-middle-text">Сфера применения:</span></div>
                                <div class="col-md-6"><span class="modal-middle-text-2">Рестораны, доставка, страхование и др.</span></div>
                              </div>
                            </div>
                          </div>
                          <div class="anketa-item-descrip-main-pay">
                            <div class="wrapper-text-mini"><span class="anketa-item-descrip-main-pay-result">19 500 &#8381;</span>
                              <div class="anketa-btn-form">
                                <button class="btn-form">Выбрать</button>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-7 anketa-item-descrip-addit">
                          <div class="wrapper-text-mini"><span class="descrip-name">Описание:
                              <div class="wrapper-descrip"><span class="descrip-text">Беспроводной POS-терминал для мобильных продаж/услуг.</span></div></span><span class="descrip-name">Основные преимущества:
                              <div class="wrapper-descrip"><span class="descrip-text">Гарантия 2 года; <br></span><span class="descrip-text">Самый маленький и легкий терминал в классе беспроводных платежных устройств; <br></span><span class="descrip-text">Стоимость включена сим-карта на 2 месяцев.</span></div></span><span class="descrip-name">Технические характеристики:
                              <div class="wrapper-descrip"><span class="descrip-text">Сертифицирован PCI PED 2.x; <br></span><span class="descrip-text">Все виды ридеров:<span class="descrip-text-info"> магнитный, чип, бесконтактный; <br></span></span><span class="descrip-text">Связь:<span class="descrip-text-info"> оффлайн, Bluetooth, GPRS, 3G; <br></span></span><span class="descrip-text">Батарея:<span class="descrip-text-info"> Li-Ion 2050 mA/h с ресурсом до 400 транзакций;<br></span></span><span class="descrip-text">Дисплей:<span class="descrip-text-info"> монохром или полноцветный;<br></span></span><span class="descrip-text">Клавиатура:<span class="descrip-text-info"> рельефная с подсветкой.</span></span></div></span><span class="descrip-name">Поставщик:
                              <div class="wrapper-descrip"><span class="descrip-text">ООО &laquo; ПЛАТИНА &raquo;</span></div></span></div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="modal-footer">
                    Настройка POS-терминала осуществляется банком.
                    За настройку каждого <br> POS-терминала взимается комиссия в размере 1000 рублей, включая НДС.
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="anketa-button">
            <div class="row">
              <div class="col-md-1">
                <button class="btn-prev" data-target="anketa-btn">Назад</button>
              </div>
              <div class="col-md-9"></div>
              <div class="col-md-2">
                <button class="btn-next" data-target="anketa-btn">Далее</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Шаг 3. Обороты и комиссия-->
    <h2>Обороты и комиссия</h2>
    <div class="step" data-step="3">
      <div class="container">
        <div class="wrapper-text-mini">
          <div class="row">
            <div class="col-md-4 anketa-money-info">
              <h4>Оборот по пластиковым картам в месяц</h4>
              <div class="input-place"><span class="input-text">Сумма</span>
                <input class="anketa-input" type="number" name="total-price" placeholder="Сумма">
                <div class="anketa-wrapper-img"><span class="anketa-wrapper-znak">?</span></div>
              </div>
              <div class="input-place"><span class="input-text">Код менеджера</span>
                <input class="anketa-input" type="number" name="manager-code" placeholder="Код менеджера">
              </div>
            </div>
            <div class="col-md-8 paysystem-table">
              <table>
                <thead>
                  <th></th>
                  <th>Платежная система</th>
                  <th>Комиссия</th>
                </thead>
                <tr>
                  <td>картинка</td>
                  <td>имя</td>
                  <td>процент</td>
                </tr>
                <tr>
                  <td>картинка</td>
                  <td>имя</td>
                  <td>процент</td>
                </tr>
                <tr>
                  <td>картинка</td>
                  <td>имя</td>
                  <td>процент</td>
                </tr>
                <tr>
                  <td>картинка</td>
                  <td>имя</td>
                  <td>процент</td>
                </tr>
                <tr>
                  <td>картинка</td>
                  <td>имя</td>
                  <td>процент</td>
                </tr>
              </table>
            </div>
          </div>
          <div class="row">
            <div class="col-md-8"><span class="anketa-help">
                Для всех указанных платежных систем срок осуществления возмещения суммдействительных операций с даты приема банком слипов, отчетов по слипам и отчетов
                электронных терминалов составляет 3 рабочих дня.</span></div>
          </div>
          <div class="row anketa-btn">
            <div class="col-md-1">
              <button class="btn-prev" data-target="anketa-btn">Назад</button>
            </div>
            <div class="col-md-9"></div>
            <div class="col-md-2" style="text-align:right;">
              <button class="btn-next" data-target="anketa-btn">Далее</button>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Шаг 4. Анкета организации-->
    <h2>Анкета организации</h2>
    <div class="step" data-step="4">
      <div class="container">
        <div class="anketa-first-row">
          <div class="wrapper-text">
            <div class="row">
              <div class="col-md-4">
                <div class="input-place"><span class="input-text">Величина уставного капитала</span>
                  <input class="anketa-input" type="number" name="total-capital" placeholder="Величина уставного капитала">
                </div>
              </div>
              <div class="col-md-4">
                <div class="input-place"><span class="input-text">Средняя штатная численность</span>
                  <input class="anketa-input" type="number" name="company-people" placeholder="Средняя штатная численность"><span class="anketa-input-help">С учетом филиалов, представительств и других подразделений</span>
                </div>
              </div>
              <div class="col-md-4">
                <div class="input-place"><span class="input-text">Индекс</span>
                  <input class="anketa-input" type="number" name="index" placeholder="Индекс">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="input-place"><span class="input-text">Юридический адрес компании</span>
                  <input class="anketa-input" type="text" name="legal-address" placeholder="Юридический адрес компании"><span class="anketa-input-help">Укажите полный адрес организации в соответствии с юридическими длокументами компании</span>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="input-place"><span class="input-text">Дата создания организации</span>
                  <input class="anketa-input" type="number" name="open-date" placeholder="Дата создания организации">
                </div>
              </div>
              <div class="col-md-4">
                <div class="input-place"><span class="input-text">Место регистрации</span>
                  <input class="anketa-input" type="number" name="registr-place" placeholder="Место регистрации">
                </div>
              </div>
              <div class="col-md-4">
                <div class="input-place"><span class="input-text">Наименование рег. органа</span>
                  <input class="anketa-input" type="number" name="registr-by" placeholder="Наименование рег. органа">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="input-place"><span class="input-text">ОГРН</span>
                  <input class="anketa-input" type="text" name="OGRN" placeholder="ОГРН">
                </div>
              </div>
              <div class="col-md-4">
                <div class="input-place"><span class="input-text">ИНН</span>
                  <input class="anketa-input" type="text" name="INN" placeholder="ИНН">
                </div>
              </div>
              <div class="col-md-4">
                <div class="input-place"><span class="input-text">КПП</span>
                  <input class="anketa-input" type="text" name="KPP" placeholder="КПП">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="input-place"><span class="input-text">ОКАТО</span>
                  <input class="anketa-input" type="text" name="OKATO" placeholder="ОКАТО">
                </div>
              </div>
              <div class="col-md-4">
                <div class="input-place"><span class="input-text">ОКПО</span>
                  <input class="anketa-input" type="text" name="OKPO" placeholder="ОКПО">
                </div>
              </div>
              <div class="col-md-4">
                <div class="input-place"><span class="input-text">ОКВЭД</span>
                  <input class="anketa-input" type="text" name="OKVED" placeholder="ОКВЭД">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="input-place"><span class="input-text">БИК</span>
                  <input class="anketa-input" type="text" name="BIK" placeholder="БИК">
                </div>
              </div>
              <div class="col-md-8">
                <div class="input-place"><span class="input-text">Наименование банка</span>
                  <input class="anketa-input" type="text" name="bank-name" placeholder="Наименование банка">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="input-place"><span class="input-text">Корреспондентский счет</span>
                  <input class="anketa-input" type="text" name="corresp-acc" placeholder="Корреспондентский счет">
                </div>
              </div>
              <div class="col-md-8">
                <div class="input-place"><span class="input-text">Расчетный счет</span>
                  <input class="anketa-input" type="text" name="check-acc" placeholder="Расчетный счет">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="anketa-second-row">
        <div class="container">
          <div class="wrapper-text">
            <div class="row">
              <div class="col-md-12"><span class="anketa-header">Торговые точки организации</span></div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="input-place"><span class="input-text">Фактическое название торговой точки</span>
                  <input class="anketa-input" type="text" name="real-name-shop" placeholder="Фактическое название торговой точки">
                </div>
              </div>
              <div class="col-md-6">
                <select class="anketa-select" name="field-activity" data-placeholder="Сфера деятельности" multiple="multiple"></select>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 anketa-true">
                <input name="anketa-step-4-lic" type="checkbox" value="license-true-yes" id="license-true">
                <label for="license-true">Деятельность организации подлежит лицензированию</label>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="input-place"><span class="input-text">Город</span>
                  <input class="anketa-input" type="text" name="city-shop" placeholder="Город">
                </div>
              </div>
              <div class="col-md-4">
                <div class="input-place"><span class="input-text">Размер торговой точки</span>
                  <input class="anketa-input" type="text" name="size-shop" placeholder="Размер торговой точки">
                </div>
              </div>
              <div class="col-md-4">
                <div class="input-place"><span class="input-text">Индекс</span>
                  <input class="anketa-input" type="text" name="index-shop" placeholder="Индекс">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="input-place"><span class="input-text">Адрес</span>
                  <input class="anketa-input" type="text" name="address-shop" placeholder="Адрес">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12"><span class="anketa-add-content">Добавить еще одну торговую точку</span></div>
            </div>
          </div>
        </div>
      </div>
      <div class="anketa-third-row">
        <div class="container">
          <div class="wrapper-text">
            <div class="row">
              <div class="col-md-12"><span class="anketa-header">Данные подписанта</span></div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="input-place"><span class="input-text">Фамилия Имя и Отчество</span>
                  <input class="anketa-input" type="text" name="FIO" placeholder="Фамилия Имя и Отчество">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <select class="anketa-select" name="status" data-placeholder="Статус подписанта" multiple="multiple"></select>
              </div>
              <div class="col-md-6">
                <select class="anketa-select" name="document" data-placeholder="Документ, определяющий полномочия" multiple="multiple"></select>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12"><span>Доверенность на представителя</span>
                <div class="dropzone" id="dropzone"></div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="input-place"><span class="input-text">Телефон</span>
                  <input class="anketa-input" type="text" name="phone" placeholder="Телефон">
                </div>
              </div>
              <div class="col-md-6">
                <div class="input-place"><span class="input-text">Email</span>
                  <input class="anketa-input" type="text" name="email" placeholder="Email">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-1">
                <button class="btn-prev" data-target="anketa-btn">Назад</button>
              </div>
              <div class="col-md-5"></div>
              <div class="col-md-6" style="text-align:right;"><span class="anketa-small-text">Нажимая кнопку «ДАЛЕЕ», я соглашаюсь с<a href=""> передачей моих данных.</a></span>
                <button class="btn-next" data-target="anketa-btn">Далее</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Шаг 5. Анкета клиента-->
    <h2>Анкета книента</h2>
    <div class="step" data-step="5">
      <div class="container">
        <div class="wrapper-text">
          <div class="row">
            <div class="col-md-12">
              <h2>Анкета бенефициарных владельцев юридического лица</h2>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 anketa-true">
              <input name="anketa-organisation" type="checkbox" value="organisation-true-yes" id="organisation-true">
              <label for="organisation-true">бенефициар является директором организации</label>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="input-place"><span class="input-text">Фамилия Имя и Отчество</span>
                <input class="anketa-input" type="text" name="FIO" placeholder="Фамилия Имя и Отчество">
              </div>
            </div>
            <div class="col-md-4">
              <div class="input-place"><span class="input-text">Дата Рождения</span>
                <input class="anketa-input" type="text" name="birth-day" placeholder="Дата Рождения">
              </div>
            </div>
            <div class="col-md-4">
              <div class="input-place"><span class="input-text">Место рождения</span>
                <input class="anketa-input" type="text" name="birth-city" placeholder="Место рождения">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="input-place"><span class="input-text">Доля участия в капитале</span>
                <input class="anketa-input" type="text" name="equity-interest" placeholder="Доля участия в капитале">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <h3>Гражданство</h3>
              <div class="switch">
                <div class="quality" data-target="anketa-russia">
                  <input id="q3" name="q1" type="radio" value="q3">
                  <label for="q3">Россия</label>
                </div>
                <div class="quality" data-target="anketa-other">
                  <input id="q4" checked="true" name="q1" type="radio" value="q4">
                  <label for="q4">Другая страна</label>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <select class="anketa-select" name="country" data-placeholder="Страна" multiple="multiple"></select>
            </div>
          </div>
          <div class="row">
            <div class="col-md-4">
              <div class="input-place"><span class="input-text">Номер карты</span>
                <input class="anketa-input" type="number" name="card-number" placeholder="Номер карты">
              </div>
            </div>
            <div class="col-md-4">
              <div class="input-place"><span class="input-text">Дата начала срока пребывания</span>
                <input class="anketa-input" type="number" name="start-date" placeholder="Дата начала срока пребывания">
              </div>
            </div>
            <div class="col-md-4">
              <div class="input-place"><span class="input-text">Дата окончания срока пребывания</span>
                <input class="anketa-input" type="number" name="finish-date" placeholder="Дата окончания срока пребывания">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <select class="anketa-select" name="support-document" data-placeholder="Тип подтверждающего документа" multiple="multiple"></select>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <h3>Паспортные данные</h3>
            </div>
            <div class="col-md-4">
              <div class="input-place"><span class="input-text">Номер</span>
                <input class="anketa-input" type="number" name="passport-number" placeholder="Номер">
              </div>
            </div>
            <div class="col-md-4">
              <div class="input-place"><span class="input-text">Дата выдачи</span>
                <input class="anketa-input" type="number" name="passport-date" placeholder="Дата выдачи">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12">
              <h3>Адрес места регистрации</h3>
            </div>
            <div class="col-md-4">
              <div class="input-place"><span class="input-text">Индекс</span>
                <input class="anketa-input" type="number" name="registr-index" placeholder="Индекс">
              </div>
            </div>
            <div class="col-md-4">
              <div class="input-place"><span class="input-text">Населенный пункт</span>
                <input class="anketa-input" type="text" name="registr-city" placeholder="Населенный пункт">
              </div>
            </div>
            <div class="col-md-4">
              <div class="input-place"><span class="input-text">Адрес</span>
                <input class="anketa-input" type="text" name="registr-address" placeholder="Адрес">
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12 anketa-true">
              <input name="anketa-step-4-lic" type="checkbox" value="license-true-yes" id="registr-place">
              <label for="registr-place">Не совпадает с адресом места жительства</label><span class="add-benefic"></span>
            </div>
          </div>
        </div>
      </div>
      <div class="anketa-other-back">
        <div class="container">
          <div class="wrapper-text">
            <div class="row">
              <div class="col-md-12">
                <h2 class="anketa-header">Анкета директора организации</h2>
              </div>
              <div class="col-md-4">
                <div class="input-place"><span class="input-text">Фамилиия Имя и Отчество</span>
                  <input class="anketa-input" type="text" name="FIO-direct" placeholder="Фамилия Имя и Отчество">
                </div>
              </div>
              <div class="col-md-4">
                <div class="input-place"><span class="input-text">Дата рождения</span>
                  <input class="anketa-input" type="number" name="birth-date-direct" placeholder="Дата рождения">
                </div>
              </div>
              <div class="col-md-4">
                <div class="input-place"><span class="input-text">Место рождения</span>
                  <input class="anketa-input" type="text" name="birth-place-direct" placeholder="Место рождения">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <h3>Гражданство</h3>
                <div class="switch">
                  <div class="quality" data-target="anketa-russia-direct">
                    <input id="q5" checked="true" name="q2" type="radio" value="q5">
                    <label for="q5">Россия</label>
                  </div>
                  <div class="quality" data-target="anketa-other-direct">
                    <input id="q6" name="q2" type="radio" value="q6">
                    <label for="q6">Другая страна</label>
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <h3>Паспортные данные</h3>
              </div>
              <div class="col-md-4">
                <div class="input-place"><span class="input-text">Серия</span>
                  <input class="anketa-input" type="number" name="passport-serial-direct" placeholder="Серия">
                </div>
              </div>
              <div class="col-md-4">
                <div class="input-place"><span class="input-text">Номер</span>
                  <input class="anketa-input" type="number" name="passport-number-direct" placeholder="Номер">
                </div>
              </div>
              <div class="col-md-4">
                <div class="input-place"><span class="input-text">Код подразделения</span>
                  <input class="anketa-input" type="number" name="passport-code-direct" placeholder="Код подразделения">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <h3>Адрес места регистрации</h3>
              </div>
              <div class="col-md-4">
                <div class="input-place"><span class="input-text">Индекс</span>
                  <input class="anketa-input" type="number" name="registr-index-direct" placeholder="Индекс">
                </div>
              </div>
              <div class="col-md-4">
                <div class="input-place"><span class="input-text">Населенный пункт</span>
                  <input class="anketa-input" type="text" name="registr-city-direct" placeholder="Населенный пункт">
                </div>
              </div>
              <div class="col-md-4">
                <div class="input-place"><span class="input-text">Адрес</span>
                  <input class="anketa-input" type="text" name="registr-address-direct" placeholder="Адрес">
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12 anketa-true">
                <input name="anketa-step-4-lic" type="checkbox" value="license-true-yes" id="registr-place-direct">
                <label for="registr-place-direct">Не совпадает с адресом места жительства</label>
              </div>
            </div>
            <div class="row">
              <div class="col-md-4">
                <div class="input-place"><span class="input-text">ИНН</span>
                  <input class="anketa-input" type="number" name="inn-direct" placeholder="ИНН">
                </div>
              </div>
              <div class="col-md-4">
                <div class="input-place"><span class="input-text">Номер телефона</span>
                  <input class="anketa-input" type="text" name="phone-number-direct" placeholder="Номер телефона">
                </div>
              </div>
              <div class="col-md-4">
                <div class="input-place"><span class="input-text">Электронная почта</span>
                  <input class="anketa-input" type="text" name="email-direct" placeholder="Электронная почта">
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="anketa-next-step-btn">
        <div class="container">
          <div class="wrapper-text">
            <div class="row anketa-btn">
              <div class="col-md-1">
                <button class="btn-prev" data-target="anketa-btn">Назад</button>
              </div>
              <div class="col-md-5"></div>
              <div class="col-md-6" style="text-align:right;">
                <button class="btn-next" data-target="anketa-btn">Далее</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!--Шаг 6. Оплата-->
    <h2>Оплата</h2>
    <div class="step" data-step="6">
      <div class="container">
        <div class="wrapper-text">
          <div class="row">
            <div class="col-md-12">
              <h3>Выберите способ оплаты</h3>
            </div>
          </div>
          <div class="row">
            <div class="col-md-12"><span>Терминалы</span></div>
          </div>
          <div class="row">
            <div class="col-md-6 pay-system pay-system-1">
              <div class="wrapper-text-mini">
                <h4>Получить счет оферту для оплаты на email</h4><span class="pay-text">
                  Счет-оферта будет направлен Вам не позднее следующего рабочего дня (10:00-18:00 / Пн-Пт).Вы можете оплатить его любым удобным способом.</span>
                <div class="anketa-btn"><span class="anketa-btn-form">
                    <button class="btn-form">Оплатить</button></span></div>
              </div>
            </div>
            <div class="col-md-6 pay-system pay-system-2">
              <div class="wrapper-text-mini">
                <h4>Оплатить картой on-line</h4><span class="pay-text">
                  Для оплаты Вам необходимо будет на защищенной странице Банка ввести данные по пластиковойкарте, денежные средства будут списаны в счет оплаты приобретаемого POS терминала.</span>
                <div class="anketa-btn"><span class="anketa-btn-form">
                    <button class="btn-form">Оплатить</button></span></div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="anketa-other-back">
        <div class="container">
          <div class="wrapper-text">
            <div class="row">
              <div class="col-md-12">
                <h4>Договор</h4><span class="pay-text">Сотрудник банка приедет в организацию для подписания документов.</span>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <h4>Доставка</h4><span class="pay-text">Терминалы в течение 3-х рабочих дней будут отправлены поставщиком на адрес торговой точки: <br></span><span class="pay-text-mini">г. Москва, ул. Академика Королева д.24, оф.1107 <br></span><span class="pay-text-mini">г. Москва, ул. Ткацкая, д.30, оф.24</span>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <h4>Подключение</h4><span class="pay-text">Настройка POS-терминалов осуществляется банком. <br></span><span class="pay-text">
                  За настройку каждого POS-терминала взимается комиссия в размере 1 000 руб, включая НДС. Счет будет направлен отдельно на почту<span class="pay-text-help">index@rsb.ru</span></span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="anketa-next-step-btn">
        <div class="container">
          <div class="wrapper-text">
            <div class="row anketa-btn">
              <div class="col-md-1">
                <button class="btn-prev" data-target="anketa-btn">Назад</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</form>
<footer>
  <div class="container">
    <div class="row">
      <div class="col-md-5 col-xs-4 footer-text">
        <div class="wrapper-text">
          <p class="footer-text-license">
            © 2016 Банк Русский Стандарт Генеральная лицензия Банка
            России
            №
            2289
            выдана бессрочно 19 ноября 2014 года
          </p>
          <ul class="footer-text-info">
            <li>
              <p>Контактная информация</p>
            </li>
            <li>
              <p>Карта сайта</p>
            </li>
          </ul>
        </div>
      </div>
      <div class="col-md-2"></div>
      <div class="col-md-3 col-xs-4 footer-phone">
        <div class="wrapper-text">
          <p class="footer-phone-number"><a href="tel:+88002006203">8-800 200-6-203</a></p>
          <p class="footer-phone-text">Звонок по России бесплатный</p>
        </div>
      </div>
      <div class="col-md-2 col-xs-4 footer-help">
        <div class="wrapper-text">
          <p class="footer-help-email"><a href="mailto:ossrp@rsb.ru">ossrp@rsb.ru</a></p>
          <p class="footer-help-text">Служба поддержки</p>
        </div>
      </div>
    </div>
  </div>
</footer>
  <script type="text/javascript" src="./custom-common-js.js"></script></body>
</html>



#anketa {
  background-color: #f7f9f8;
  margin-top: 23px;
}
.wrapper-text {
  padding: 50px 43px 20px 43px;
}
.wrapper-text .anketa-step-1-data-name {
  font-size: 12px;
  padding-left: 2px;
  font-family: 'LatoMedium', sans-serif;
}
.wrapper-text .anketa-small-text {
  font-size: 11px;
  font-family: 'LatoMedium', sans-serif;
}
.wrapper-text .anketa-small-text a {
  color: #83b143;
  text-decoration: none;
}
.wrapper-text .anketa-next-step-btn {
  background-color: #f7f9f8;
  margin-top: 30px;
}
.anketa-input:focus::-webkit-input-placeholder {
  color: transparent;
}
.anketa-input:focus::-moz-placeholder {
  color: transparent;
}
.anketa-input:-moz-placeholder {
  color: transparent;
}
.input-place,
.select-place {
  position: relative;
}
.input-text {
  position: absolute;
  top: 6px;
  left: 5px;
  transition: .3s;
  font-size: 14px;
  font-family: 'LatoMedium', sans-serif;
  color: #a2a4a3;
  display: none;
}
.input-text-active {
  top: -17px;
  font-size: 12px;
  color: #989a99;
  display: block;
}
.anketa-input-help {
  font-size: 12px;
  width: 100%;
  font-family: 'LatoMedium', sans-serif;
  position: absolute;
  left: 0;
  top: 32px;
  color: #a2a4a3;
}
input:focus::-webkit-input-placeholder {
  color: transparent;
}
h2,
h3,
h4 {
  font-family: 'LatoMedium', sans-serif;
}
h2 {
  font-size: 24px;
}
h3 {
  font-size: 20px;
}
h4 {
  font-size: 17px;
}
.wrapper-text {
  padding: 50px 43px 8px 43px;
}
.wrapper-text .anketa-input-help {
  font-size: 12px;
  width: 100%;
  font-family: 'LatoMedium', sans-serif;
  top: 32px;
  left: 0;
  position: absolute;
}
.wrapper-text #anketa-answer-no {
  margin-top: 30px;
}
.wrapper-text #anketa-answer-no .row-content {
  margin-bottom: 6px;
}
.wrapper-text #anketa-answer-no .row-content .anketa-name-col {
  font-size: 12px;
  padding-left: 2px;
  font-family: 'LatoMedium', sans-serif;
}
.wrapper-text #anketa-answer-no .row-content .anketa-name-col.anketa-name-col-center {
  text-align: center;
}
.wrapper-text #anketa-answer-no .anketa-pay-total {
  font-family: 'LatoMedium', sans-serif;
  font-size: 18px;
  text-align: right;
  display: block;
}
.wrapper-text #anketa-answer-no .anketa-pay-total .anketa-pay-total-rubles {
  color: #83b143;
}
.wrapper-text #anketa-answer-no .wrapper-circle-help {
  display: inline-block;
  height: 20px;
  width: 20px;
  border: 1px solid #83b143;
  border-radius: 10px;
  cursor: pointer;
  margin-left: 8px;
  position: absolute;
  right: -8px;
  top: 12px;
}
.wrapper-text #anketa-answer-no .wrapper-circle-help:hover .anketa-choose-help-text,
.wrapper-text #anketa-answer-no .wrapper-circle-help:hover:after {
  display: block;
}
.wrapper-text #anketa-answer-no .wrapper-circle-help:after {
  content: '';
  position: absolute;
  left: 0px;
  bottom: -16px;
  border: 10px solid transparent;
  border-bottom: 10px solid #83b143;
  display: none;
  transition: .6s;
}
.wrapper-text #anketa-answer-no .wrapper-circle-help .anketa-choose-help {
  position: absolute;
  left: 0;
  right: 0;
  width: 8px;
  margin: auto;
  color: #83b143;
}
.wrapper-text #anketa-answer-no .wrapper-circle-help .anketa-choose-help-text {
  display: none;
  transition: .6s;
  position: absolute;
  left: -18px;
  width: 300px;
  background-color: white;
  color: black;
  top: 35px;
  z-index: 9999;
  box-shadow: 1px 1px 1px black;
  padding: 20px;
  border-radius: 3px;
}
.wrapper-text #anketa-answer-no .anketa-item-row {
  background-color: white;
  margin-bottom: 25px;
}
.wrapper-text #anketa-answer-no .anketa-item-row:hover {
  box-shadow: 1px 5px 15px #e9ebe9;
}
.wrapper-text #anketa-answer-no .anketa-item-row .anketa-item {
  padding: 33px 20px 70px 20px;
}
.wrapper-text #anketa-answer-no .anketa-item-row .anketa-item .anketa-shop {
  position: relative;
  padding-left: 40px;
}
.wrapper-text #anketa-answer-no .anketa-item-row .anketa-item .anketa-shop label:after {
  content: '';
  display: block;
  height: 25px;
  width: 25px;
  border: 1px solid #939598;
  position: absolute;
  top: 0;
  left: 0;
  border-radius: 3px;
}
.wrapper-text #anketa-answer-no .anketa-item-row .anketa-item .anketa-shop input[type=checkbox] {
  display: none;
}
.wrapper-text #anketa-answer-no .anketa-item-row .anketa-item .anketa-shop input[type=checkbox]:checked + label:after {
  width: 25px;
  height: 25px;
  background-color: #83b143;
  content: '\2714';
  text-align: center;
  color: white;
}
.wrapper-text #anketa-answer-no .anketa-item-row .anketa-item .anketa-name-terminal {
  font-family: 'LatoBold', sans-serif;
  font-size: 18px;
}
.wrapper-text #anketa-answer-no .anketa-item-row .anketa-item .anketa-name-terminal span {
  font-size: 12px;
  padding-left: 2px;
  font-family: 'LatoMedium', sans-serif;
}
.wrapper-text #anketa-answer-no .anketa-item-row .anketa-item .anketa-name-terminal-info {
  color: #83b143;
  font-size: 16px;
  display: inline;
  font-family: 'LatoRegular', sans-serif;
  display: block;
}
.wrapper-text #anketa-answer-no .anketa-item-row .anketa-item .anketa-name-terminal-info:hover {
  cursor: pointer;
}
.wrapper-text #anketa-answer-no .anketa-item-row .anketa-item .anketa-change-kol {
  font-family: 'LatoBold', sans-serif;
  font-size: 18px;
  color: #83b143;
  cursor: pointer;
}
.wrapper-text #anketa-answer-no .anketa-item-row .anketa-item .anketa-change-item {
  font-family: 'LatoBold', sans-serif;
  font-size: 18px;
  margin: 0 20px;
}
.wrapper-text #anketa-answer-no .anketa-item-row .anketa-item .anketa-totalcost,
.wrapper-text #anketa-answer-no .anketa-item-row .anketa-item .anketa-totalcost-rubles {
  font-family: 'LatoBold', sans-serif;
  font-size: 18px;
}
.wrapper-text #anketa-answer-no .anketa-item-row .anketa-item .anketa-totalcost-formula {
  font-family: 'LatoMedium', sans-serif;
  font-size: 12px;
  color: #a1a1a1;
  display: block;
}
.wrapper-text #anketa-answer-no .modal-dialog {
  width: 800px;
  margin: 30px auto;
}
.wrapper-text #anketa-answer-no .modal-dialog .anketa-name-terminal {
  font-family: 'LatoBold', sans-serif;
  font-size: 18px;
  text-align: center;
}
.wrapper-text #anketa-answer-no .modal-dialog .anketa-name-terminal span {
  font-size: 12px;
  padding-left: 2px;
  font-family: 'LatoMedium', sans-serif;
}
.wrapper-text #anketa-answer-no .modal-dialog .anketa-item-pic {
  display: inline-block;
  position: relative;
  width: 100%;
}
.wrapper-text #anketa-answer-no .modal-dialog .anketa-item-pic-curr {
  display: inline-block;
}
.wrapper-text #anketa-answer-no .modal-dialog .anketa-item-descrip {
  margin-bottom: 30px;
}
.wrapper-text #anketa-answer-no .modal-dialog .anketa-item-descrip .wrapper-text {
  padding: 20px 15px;
}
.wrapper-text #anketa-answer-no .modal-dialog .anketa-item-descrip .wrapper-text .anketa-item-descrip-main {
  background-color: #f4f4f6;
  border-radius: 3px;
  height: max-content;
}
.wrapper-text #anketa-answer-no .modal-dialog .anketa-item-descrip .wrapper-text .anketa-item-descrip-main .anketa-btn-form {
  text-align: center;
}
.wrapper-text #anketa-answer-no .modal-dialog .anketa-item-descrip .wrapper-text .anketa-item-descrip-main .anketa-btn-form .btn-form {
  margin-top: 10px;
}
.wrapper-text #anketa-answer-no .modal-dialog .anketa-item-descrip .wrapper-text .anketa-item-descrip-main .wrapper-text-mini {
  padding: 15px;
}
.wrapper-text #anketa-answer-no .modal-dialog .anketa-item-descrip .wrapper-text .anketa-item-descrip-main .anketa-item-descrip-main-info {
  position: relative;
  display: inline-block;
  width: 100%;
}
.wrapper-text #anketa-answer-no .modal-dialog .anketa-item-descrip .wrapper-text .anketa-item-descrip-main .anketa-item-descrip-main-info:after {
  position: absolute;
  bottom: 0;
  left: 0;
  right: 0;
  margin: auto;
  width: 100%;
  background-color: #e0e1e3;
  height: 1px;
  content: '';
}
.wrapper-text #anketa-answer-no .modal-dialog .anketa-item-descrip .wrapper-text .anketa-item-descrip-addit .wrapper-text-mini {
  padding: 15px;
}
.wrapper-text #anketa-answer-no .modal-dialog .anketa-item-descrip .wrapper-text .anketa-item-descrip-addit .wrapper-text-mini .descrip-name {
  font-family: 'LatoBold', sans-serif;
  font-size: 18px;
}
.wrapper-text #anketa-answer-no .modal-dialog .anketa-item-descrip .wrapper-text .anketa-item-descrip-addit .wrapper-text-mini .descrip-name .wrapper-descrip {
  padding: 10px 0 10px 20px;
}
.wrapper-text #anketa-answer-no .modal-dialog .anketa-item-descrip .wrapper-text .anketa-item-descrip-addit .wrapper-text-mini .descrip-name .wrapper-descrip .descrip-text {
  font-family: 'LatoMedium', sans-serif;
  font-size: 14px;
  color: #8f8f8f;
}
.wrapper-text #anketa-answer-no .modal-dialog .anketa-item-descrip .wrapper-text .anketa-item-descrip-addit .wrapper-text-mini .descrip-name .wrapper-descrip .descrip-text .descrip-text-info {
  color: #333333;
}
.wrapper-text #anketa-answer-no .modal-dialog .anketa-item-descrip-main-pay-result {
  font-family: 'LatoMedium', sans-serif;
  font-size: 30px;
  display: block;
  text-align: center;
}
.wrapper-text #anketa-answer-no .modal-dialog .modal-middle-text {
  font-family: 'LatoMedium', sans-serif;
  color: #858585;
  font-size: 12px;
  position: relative;
}
.wrapper-text #anketa-answer-no .modal-dialog .modal-middle-text .anketa-wrapper-img {
  position: absolute;
  height: 16px;
  width: 16px;
  border-radius: 50%;
  border: 1px solid #83b143;
  right: -20%;
  top: -2px;
}
.wrapper-text #anketa-answer-no .modal-dialog .modal-middle-text .anketa-wrapper-img .anketa-wrapper-znak {
  color: #83b143;
  left: 0;
  right: 0;
  text-align: center;
  position: absolute;
}
.wrapper-text #anketa-answer-no .modal-dialog .modal-middle-text-2 {
  color: black;
  font-family: 'LatoBold', sans-serif;
  font-size: 12px;
}
.wrapper-text #anketa-answer-no .modal-dialog .anketa-help {
  left: 78px;
  width: 75%;
}
.wrapper-text #anketa-answer-no .modal-dialog .modal-footer {
  text-align: left;
  padding: 15px 18%;
  position: relative;
}
.display-none {
  display: none;
}
.switch {
  background-color: white;
  position: relative;
  margin: 10px auto;
  width: 100%;
  height: 40px;
  border: 1px solid #f2f4f3;
  font-family: 'LatoMedium', sans-serif;
  color: black;
  font-size: 14px;
  border-radius: 20px;
  z-index: 999;
  box-shadow: 1px 4px 6px #f3f4f3;
}
.quality {
  position: relative;
  width: 50%;
  height: 100%;
  line-height: 40px;
  float: left;
}
.quality label {
  border-radius: 18px;
}
.quality label {
  position: absolute;
  top: -1px;
  left: 0;
  width: 123%;
  height: 105%;
  cursor: pointer;
  text-align: center;
  transition: transform 0.4s, color 0.4s, background-color 0.4s;
  font-weight: normal;
}
.quality input[type="radio"] {
  appearance: none;
  width: 0;
  height: 0;
  opacity: 0;
}
.quality input[type="radio"]:focus {
  outline: 0;
  outline-offset: 0;
}
.quality input[type="radio"]:checked ~ label {
  background-color: #83b143;
  color: white;
}
.quality input[type="radio"]:active ~ label {
  transform: scale(1.05);
}
.wrapper-text-mini {
  padding: 50px 43px;
}
.wrapper-text-mini .anketa-money-info .anketa-wrapper-img {
  position: absolute;
  height: 20px;
  width: 20px;
  border-radius: 10px;
  border: 1px solid #83b143;
  right: 12%;
  top: 0;
}
.wrapper-text-mini .anketa-money-info .anketa-wrapper-img .anketa-wrapper-znak {
  color: #83b143;
  left: 0;
  right: 0;
  text-align: center;
  position: absolute;
}
.wrapper-text-mini .anketa-money-info .anketa-money-info-header {
  font-family: 'LatoMedium', sans-serif;
  font-size: 17px;
  display: block;
}
.wrapper-text-mini .anketa-money-info .anketa-input-header {
  font-size: 12px;
  padding-left: 2px;
  font-family: 'LatoMedium', sans-serif;
  margin-bottom: 0;
}
.wrapper-text-mini .anketa-money-info .anketa-input {
  border: none;
  border-bottom: 1px solid #cbd5d7;
  background-color: transparent;
  outline: none;
  width: 80%;
  margin-bottom: 20px;
  font-size: 15px;
  font-family: 'LatoRegular', sans-serif;
}
.wrapper-text-mini .anketa-money-info .input-text {
  top: 4px;
}
.wrapper-text-mini .anketa-money-info .input-text-active {
  top: -10px;
}
.wrapper-text-mini .paysystem-table table {
  width: 100%;
  font-family: 'LatoRegular', sans-serif;
}
.wrapper-text-mini .paysystem-table table thead {
  color: #979998;
  text-transform: uppercase;
  font-size: 11px;
  padding-bottom: 15px;
}
.wrapper-text-mini .paysystem-table table tbody {
  background-color: white;
  border-radius: 5px;
}
.wrapper-text-mini .paysystem-table table tbody td {
  padding: 10px 20px;
  position: relative;
}
.wrapper-text-mini .paysystem-table table tbody td:after {
  position: absolute;
  content: '';
  background-color: #dbe1e1;
  bottom: 0;
  height: 1px;
  width: 100%;
  left: 0;
  right: 0;
  margin: auto;
}
.wrapper-text-mini .paysystem-table table tbody td:nth-child(1):after {
  width: 95%;
  left: 10px;
}
.wrapper-text-mini .paysystem-table table tbody td:nth-last-child(1):after {
  width: 95%;
  right: 10px;
}
.wrapper-text-mini .paysystem-table table tbody tr:nth-last-child(1) td:after {
  height: 0;
}
.wrapper-text-mini .anketa-wrapper {
  position: relative;
  display: inline-block;
  height: 30px;
  width: 30px;
  background-color: #efc415;
  border-radius: 15px;
}
.wrapper-text-mini .anketa-wrapper:after {
  position: absolute;
  left: 0;
  right: 0;
  margin: auto;
  color: white;
  display: inline;
  top: 14%;
  width: 2px;
  content: "!";
}
input::-webkit-outer-spin-button,
input::-webkit-inner-spin-button {
  /* display: none; <- Crashes Chrome on hover */
  -webkit-appearance: none;
  margin: 0;
  /* <-- Apparently some margin are still there even though it's hidden */
}
.anketa-first-row .wrapper-text {
  padding: 50px 43px 8px 43px;
}
.anketa-first-row .wrapper-text .anketa-input-help-text {
  font-size: 12px;
  padding-left: 2px;
  font-family: 'LatoMedium', sans-serif;
  margin-bottom: 0;
}
.anketa-first-row .wrapper-text .anketa-input-help-text.anketa-info-footer {
  margin-top: -35px;
}
.anketa-first-row .wrapper-text .anketa-input-help-text.anketa-4-legal-adress-footer {
  margin-bottom: 30px;
}
.anketa-second-row {
  background-color: #f4f4f6;
}
.anketa-second-row .wrapper-text {
  padding: 50px 43px;
}
.anketa-second-row .wrapper-text .anketa-header {
  font-family: 'LatoMedium', sans-serif;
  font-size: 18px;
  display: block;
}
.anketa-second-row .wrapper-text .anketa-input {
  border: none;
  border-bottom: 1px solid #cbd5d7;
  background-color: transparent;
  outline: none;
  font-size: 15px;
  width: 100%;
  font-family: 'LatoRegular', sans-serif;
}
.anketa-second-row .wrapper-text .anketa-input.anketa-4-place-field {
  margin-top: 6px;
}
.anketa-second-row .wrapper-text .anketa-add-content {
  cursor: pointer;
}
.anketa-second-row .wrapper-text .anketa-true {
  position: relative;
  padding-left: 50px;
}
.anketa-second-row .wrapper-text .anketa-true label:after {
  content: '';
  display: block;
  height: 25px;
  width: 25px;
  border: 1px solid #939598;
  position: absolute;
  top: 0;
  left: 19px;
  border-radius: 3px;
}
.anketa-second-row .wrapper-text .anketa-true input[type=checkbox] {
  display: none;
}
.anketa-second-row .wrapper-text .anketa-true input[type=checkbox]:checked + label:after {
  width: 25px;
  height: 25px;
  background-color: #83b143;
  content: '\2714';
  text-align: center;
  color: white;
}
.anketa-third-row .wrapper-text {
  padding: 50px 43px;
}
.anketa-third-row .wrapper-text .anketa-header {
  font-family: 'LatoMedium', sans-serif;
  font-size: 18px;
  display: block;
}
.anketa-third-row .wrapper-text .anketa-small-text {
  font-size: 11px;
  font-family: 'LatoMedium', sans-serif;
}
.anketa-third-row .wrapper-text .anketa-small-text a {
  color: #83b143;
  text-decoration: none;
}
.anketa-true {
  position: relative;
  padding-left: 50px;
  margin-top: 20px;
}
.anketa-true label:after {
  content: '';
  display: block;
  height: 25px;
  width: 25px;
  border: 1px solid #939598;
  position: absolute;
  top: 0;
  left: 19px;
  border-radius: 3px;
}
.anketa-true input[type=checkbox] {
  display: none;
}
.anketa-true input[type=checkbox]:checked + label:after {
  width: 25px;
  height: 25px;
  background-color: #83b143;
  content: '\2714';
  text-align: center;
  color: white;
}
.wrapper-text {
  padding-top: 20px;
}
.anketa-other-back {
  background-color: #f4f4f6;
}
.pay-system {
  border: 1px solid black;
  position: relative;
  width: 47%;
}
.pay-system:hover {
  box-shadow: 7px 15px 22px rgba(30, 30, 30, 0.2);
}
.pay-system:hover:before {
  position: absolute;
  height: 10px;
  background-color: #83b143;
  width: 100%;
  left: 0;
  right: 0;
  margin: auto;
  top: 0;
  content: '';
}
.pay-system-1 {
  margin-right: 15px;
}
.pay-system-2 {
  margin-left: 15px;
}
.pay-text {
  font-family: 'LatoRegular', sans-serif;
  font-size: 14px;
}
.pay-text .pay-text-help {
  color: #83b143;
}
.pay-text-mini {
  font-size: 10px;
  opacity: .8;
  text-transform: uppercase;
}
.title.current,
.title,
ul[role=tablist],
li[role=tab] {
  display: none;
}
.btn-next,
.btn-prev,
.btn-form {
  background-color: #83b143;
  border-radius: 20px;
  border: none;
  padding: 10px 0;
  color: white;
  font-family: 'LatoMedium', sans-serif;
  font-size: 14px;
  display: inline-block;
  margin-top: 23px;
  cursor: pointer;
  width: 140px;
  box-shadow: 7px 15px 22px rgba(30, 30, 30, 0.2);
}
.btn-next span,
.btn-prev span,
.btn-form span {
  margin-left: 0;
  position: relative;
  font-size: 16px;
}
.btn-next:focus,
.btn-prev:focus,
.btn-form:focus {
  outline: none;
}
.btn-next:hover,
.btn-prev:hover,
.btn-form:hover {
  box-shadow: 7px 15px 22px rgba(30, 30, 30, 0.2);
  background-color: #71a02e;
}
.btn-next:active,
.btn-prev:active,
.btn-form:active {
  background-color: #71a02e;
  box-shadow: 7px 8px 22px rgba(30, 30, 30, 0.2);
}
#anketa .row {
  margin-bottom: 40px;
}
.fallback {
  width: 100%;
  display: inline-block;
  height: 200px;
  border: 1px dotted black;
}
a {
  color: #83b143;
  text-decoration: none;
}
span {
  font-family: 'LatoMedium', sans-serif;
}
.anketa-help {
  position: relative;
  left: 28px;
  font-size: 12px;
  font-family: 'LatoMedium', sans-serif;
  color: #787a79;
}
.anketa-help:before {
  position: absolute;
  height: 30px;
  width: 30px;
  background-color: #efc415;
  border-radius: 15px;
  content: '!';
  line-height: 28px;
  text-align: center;
  left: -33px;
  margin: auto;
  color: white;
  display: inline;
  top: 4%;
}
.anketa-input::placeholder {
  font-size: 14px;
}
.anketa-select {
  border: none;
  border-bottom: 1px solid #cbd5d7;
  background-color: transparent;
  outline: none;
  width: 100%;
  font-size: 15px;
  padding: 1px 0;
  font-family: 'LatoRegular', sans-serif;
  margin-top: 16px;
}
.anketa-input {
  border: none;
  border-bottom: 1px solid #cbd5d7;
  background-color: transparent;
  outline: none;
  width: 100%;
  font-size: 15px;
  padding: 0;
  font-family: 'LatoRegular', sans-serif;
  line-height: 3rem;
}
.anketa .anketa-name {
  padding-top: 27px;
}
.anketa .anketa-name .anketa-name-text {
  padding-left: 43px;
  font-size: 34px;
  font-family: 'LatoRegular', sans-serif;
  margin-top: 35px;
  color: #50514f;
}
.anketa .anketa-step-1 .anketa-step-menu {
  display: inline-block;
  position: relative;
  padding-bottom: 30px;
  padding-top: 30px;
  margin: 0;
}
.anketa .anketa-step-1 .anketa-step-menu:after {
  position: absolute;
  bottom: 0;
  width: 96%;
  background-color: black;
  height: 2px;
  content: '';
}
.anketa .anketa-step-1 .anketa-step-menu:before {
  position: absolute;
  top: 0;
  width: 92%;
  background-color: black;
  height: 8px;
  content: '';
  border-radius: 4px;
  left: 66px;
}
.anketa .anketa-step-1 .anketa-step-menu li {
  display: inline-block;
  list-style: none;
  text-decoration: none;
  padding: 0 27px;
  font-size: 15px;
  font-family: 'LatoRegular', sans-serif;
  position: relative;
}
.anketa .anketa-step-1 .anketa-step-menu li.anketa-step-menu-active {
  font-family: 'LatoBold', sans-serif;
}
.anketa .anketa-step-1 .anketa-step-menu li:before {
  position: absolute;
  height: 8px;
  width: 1px;
  background-color: black;
  top: -9px;
  left: 0;
  right: 0;
  margin: auto;
  content: '';
}





$(document).ready(function() {
    var data = [
        {
            id: 0,
            text: 'Москва'
        },
        {
            id: 1,
            text: 'Санкт-Петербург'
        },
        {
            id: 2,
            text: 'Самара'
        },
        {
            id: 3,
            text: 'Казань'
        },
        {
            id: 4,
            text: 'Екатеринбург'
        },
        {
            id: 5,
            text: 'Калининград'
        },{
            id: 6,
            text: 'Сочи'
        },
        {
            id: 7,
            text: 'Рязань'
        },
        {
            id: 8,
            text: 'Калуга'
        },
        {
            id: 9,
            text: 'Нижний Новгород'
        },
        {
            id: 10,
            text: 'Пермь'
        },
        {
            id: 11,
            text: 'Иркутск'
        }
    ];



    $('#anketa').steps({
        headerTag: 'h2',
        bodyTag: 'div',
        enableKeyNavigation: false,
        enablePagination: false,
        transitionEffect: 1
    });
    var self = this;
    self.$wizard = $('#anketa');

    self.$wizard.find('.btn-next').on('click', function (e) {
        e.preventDefault();
        self.$wizard.steps("next");
    });
    self.$wizard.find('.btn-prev').on('click', function (e) {
        e.preventDefault();
        self.$wizard.steps('previous');
    });
    $('.anketa-select').select2({
        width: '100%',
        maximumSelectionLength: 1,
        allowClear: true,
        data: data
    })




// текст из инпута уезжает наверх
$('.anketa-input')
.focus(function() {
    $(this).closest('.input-place').find('.input-text').addClass('input-text-active')

})
.blur(function() {
    if ($(this).val().length==0) {
        $(this).closest('.input-place').find('.input-text').removeClass('input-text-active')
    }
});

$('.select2-selection__rendered').click(function() {
    $(this).closest('.select-place').find('.input-text').addClass('input-text-active')

    });


    // $('.anketa-input')
    // .select2('open', function() {
    //     $(this).closest('.select-place').find('.input-text').addClass('input-text-active')
    // })
    // .select2("close", function() {
    //     $(this).closest('.select-place').find('.input-text').removeClass('input-text-active')
    // })


    //
    // if (!($('.select2-selection__rendered li').hasClass('select2-selection__choice'))) {
    //     $(this).closest('.input-place').find('.input-text').removeClass('input-text-active')
    // }


//изменение количества терминалов
var totalItems = 0;

// $('.anketa-change-add').click(function () {
//     var totalCost = +$('.anketa-totalcost').attr('data-price')
//     totalItems = totalItems + 1;
//     totalCost = totalCost * totalItems
//     console.log(totalItems);
//     console.log(totalCost);
//     if (totalItems >=2 ) {
//         $(this).closest('.anketa-item-row').find('.anketa-totalcost-formula').text(totalItems + ' x ' +  +$('.anketa-totalcost').attr('data-price'))
//     }
//     $(this).closest('.anketa-item-row').find('.anketa-change-item').text(totalItems)
//     $(this).closest('.anketa-item-row').find('.anketa-totalcost').text(totalCost)
//     totalCost = +$('.anketa-totalcost').attr('data-price')
//     return totalItems
// });
//
// $('.anketa-change-delete').click(function () {
//     totalItems = totalItems - 1;
//     totalCost = totalCost * totalItems
//     if (totalItems < 0) {
//         totalItems = 0;
//     }
//     if (totalItems == 0 ) {
//         totalCost = +$('.anketa-totalcost').attr('data-price')
//     }
//     if (totalItems >=2 ) {
//         $(this).closest('.anketa-item-row').find('.anketa-totalcost-formula').text(totalItems + ' x ' +  +$('.anketa-totalcost').attr('data-price'))
//     }
//     else {
//         $(this).closest('.anketa-item-row').find('.anketa-totalcost-formula').empty()
//     }
//
//     $(this).closest('.anketa-item-row').find('.anketa-change-item').text(totalItems)
//     $(this).closest('.anketa-item-row').find('.anketa-totalcost').text(totalCost)
//     totalCost = +$('.anketa-totalcost').attr('data-price')
//     console.log(totalItems);
// });


    $(document).ready(function() {
        $("#dropzone").dropzone({
            url: "/file/post",
            dictDefaultMessage: 'Загрузите фото или скан-копию документа<br>перетащить сюда или <a href="#">выбрать</a>'
        });
    });


//переключатель
$('.quality').click(function () {
    var elem = $(this)
    var curTab = elem.attr('data-target')
    console.log(curTab)
    var tabsParent = elem.closest('.wrapper-text')
    tabsParent.find('.anketa-row').addClass('display-none')
    tabsParent.find('#' + curTab).removeClass('display-none')
});

        });

